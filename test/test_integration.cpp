/** @file */
#include <sstream>
#include <vector>

#include <gtest/gtest.h>
#include <distance/distance.hpp>
#include <distance/boost/process.hpp>

using namespace distance;

namespace bp = boost::process;

/**
 * This class contains the fixture and helper methods to
 * do the integration tests between the library and
 * the command-line tool.
 */
class IntegrationTest : public ::testing::Test
{
protected:

    /**  
     * This method will setup the fixture initialization.
     */
    virtual void SetUp()
    {
        mExitStatus = 0;
        mExited = false;
        mArgs.push_back(k_application_path);
    }

    /**  
     * Adds an argument to execute the command-line tool.
     *
     * @param arg The argument to add to the execution.
     */
    void addArg(const std::string &arg)
    { mArgs.push_back(arg); }

    /**
     * This method will return the exit status of the process.
     *
     * @return The exit status code.
     */
    int exitStatus() const
    { return mExitStatus; }

    /**
     * This method will return info if the process has exited or not.
     *
     * @return The exist status.
     */
    bool exited() const
    { return mExited; }

    /**
     * This method will return the entire output string from the stdout
     * of the executed process.
     *
     * @return The entire stdout string output.
     */
    std::string output() const
    { return mOutput; }

    /**
     * This method will execute the process using the specified arguments,
     * it will also check for the exit status and exit status code.
     *
     * @param status The expected exit status code.
     */
    void executeProcess(int status)
    {
        bp::context ctx; 
        ctx.stdout_behavior = bp::capture_stream(); 

        bp::child c = bp::launch(k_application_path, mArgs, ctx); 
        bp::status s = c.wait(); 

        bp::pistream &is = c.get_stdout(); 
        std::stringstream ss;
        ss << is.rdbuf();

        mExited = s.exited();
        mExitStatus = s.exit_status();

        assertExit(status);
        mOutput = ss.str();
    }

    /**
     * This method will check if the output captured from the executed
     * process contains the specified string.
     *
     * @param contains The string to find on the output.
     */
    void assertOutputContains(const std::string &contains)
    {
        const bool found = mOutput.find(contains) != std::string::npos;
        ASSERT_TRUE(found);
    }

    /**
     * This method checks if the process exited and also if the
     * exit status code matches the status code specified.
     *
     * @param status The expected status code.
     */
    void assertExit(int status)
    {
        ASSERT_TRUE(mExited) << "Process didn't exit !";
        ASSERT_EQ(mExitStatus, status);
    }

protected:
    static const std::string k_application_path;
    std::vector<std::string> mArgs;
    std::string mOutput;
    int mExitStatus;
    bool mExited;
};

const std::string IntegrationTest::k_application_path = \
    "calc_distance";


TEST_F(IntegrationTest, HelpTest)
{
    addArg("--help");
    executeProcess(EXIT_FAILURE);
    assertOutputContains("Allowed options");
}

TEST_F(IntegrationTest, HammingAlone)
{
    addArg("--hamming");
    executeProcess(EXIT_FAILURE);
    assertOutputContains("Allowed options");
}

TEST_F(IntegrationTest, StringAlone)
{
    addArg("-s");
    addArg("string");
    executeProcess(EXIT_FAILURE);
    assertOutputContains("Allowed options");
}


TEST_F(IntegrationTest, HammingMissingArgument)
{
    addArg("--hamming");
    addArg("-s");

    executeProcess(EXIT_FAILURE);
    assertOutputContains("is missing");
}


TEST_F(IntegrationTest, HammingOneString)
{
    addArg("--hamming");
    addArg("-s");
    addArg("one string");

    executeProcess(EXIT_FAILURE);
    assertOutputContains("Error");
}

TEST_F(IntegrationTest, HammingDifference)
{
    addArg("--hamming");
    addArg("-s");
    addArg("one");
    addArg("-s");
    addArg("two");

    executeProcess(EXIT_SUCCESS);
    assertOutputContains("Distance: 3");
}

TEST_F(IntegrationTest, HammingEqual)
{
    addArg("--hamming");
    addArg("-s");
    addArg("one");
    addArg("-s");
    addArg("one");

    executeProcess(EXIT_SUCCESS);
    assertOutputContains("Distance: 0");
}

TEST_F(IntegrationTest, HammingWihSpaces)
{
    addArg("--hamming");
    addArg("-s");
    addArg("one   ");
    addArg("-s");
    addArg("one   ");

    executeProcess(EXIT_SUCCESS);
    assertOutputContains("Distance: 0");
}

TEST_F(IntegrationTest, HammingWihSpacesDifference)
{
    addArg("--hamming");
    addArg("-s");
    addArg("one  a");
    addArg("-s");
    addArg("one  b");

    executeProcess(EXIT_SUCCESS);
    assertOutputContains("Distance: 1");
}

int main(int argc, char *argv[])
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
