#include <memory>
#include <vector>

#include <gtest/gtest.h>
#include <distance/distance.hpp>

using namespace distance;

TEST(BasicTest, GoodCase)
{
    const std::string blob1("abcdef");
    const std::string blob2("abcdef");
    const int ret = hamming_distance(std::begin(blob1),
                                     std::begin(blob2),
                                     blob1.size());
    EXPECT_EQ(ret, 0);
}

TEST(BasicTest, DifferentSizes)
{
    const std::string blob1("abcdef");
    const std::string blob2("abcdefghi");
    const int ret = hamming_distance(std::begin(blob1),
                                     std::begin(blob2),
                                     blob1.size());
    EXPECT_EQ(ret, 0);
}

TEST(BasicTest, IntegerType)
{
    const int set1[] = {1, 2, 3};
    const int set2[] = {1, 2, 3};
    const int size = sizeof(set1) / sizeof(set1[0]);

    const int ret = hamming_distance(set1, set2,
                                     size);
    EXPECT_EQ(ret, 0);
}


TEST(BasicTest, BoolType)
{
    const bool set1[] = {true, false, true};
    const bool set2[] = {true, false, true};
    const int size = sizeof(set1) / sizeof(set1[0]);

    const int ret = hamming_distance(set1, set2,
                                     size);
    EXPECT_EQ(ret, 0);
}

TEST(BasicTest, BoolTypeDifferent)
{
    const bool set1[] = {false, true, false};
    const bool set2[] = {true, false, true};
    const int size = sizeof(set1) / sizeof(set1[0]);

    const int ret = hamming_distance(set1, set2,
                                     size);
    EXPECT_EQ(ret, 3);
}

TEST(BasicTest, HeapBlob)
{
    const int blob_size = 1024;
    std::unique_ptr<const char> blob1(new char[blob_size]());
    std::unique_ptr<const char> blob2(new char[blob_size]());

    const int ret = hamming_distance(blob1.get(),
                                     blob2.get(),
                                     blob_size);
    EXPECT_EQ(ret, 0);
}

TEST(BasicTest, Vector)
{
    std::vector<int> set1 = {1, 2, 3, 4};
    std::vector<int> set2 = {1, 2, 3, 10};
    const int ret = hamming_distance(std::begin(set1),
                                     std::begin(set2),
                                     set1.size());
    EXPECT_EQ(ret, 1);
}

TEST(BasicTest, IntegralType)
{
    int v1 = 42;
    int v2 = 42;
    
    const int ret = hamming_distance(v1, v2);
    EXPECT_EQ(ret, 0);
}

TEST(BasicTest, IntegralTypeDifference)
{
    int v1 = 42;
    int v2 = 43;
    
    const int ret = hamming_distance(v1, v2);
    EXPECT_EQ(ret, 1);
}

TEST(BasicTest, InvalidSize)
{
    const bool set1[] = {false, true, false};
    const bool set2[] = {true, false, true};

    try
    {
        hamming_distance(set1, set2, 0);
        FAIL() << "Expected std::invalid_argument";
    }
    catch(std::invalid_argument const & err)
    {
        EXPECT_EQ(err.what(), std::string("invalid input size"));
    }
    catch(...)
    {
        FAIL() << "Expected std::invalid_argument";
    }
}

int main(int argc, char *argv[])
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}