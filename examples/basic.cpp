#include <iostream>

#include <distance/distance.hpp>

namespace dt = distance;

int main(int argc, char *argv[])
{
	std::string first("aaaa");
	std::string second("aabb");

	int dist = dt::hamming_distance(std::begin(first),
		                            std::begin(second),
		                            second.size());

	std::cout << "Hamming Distance: "
	          << dist << std::endl;
	
	return 0;
}
