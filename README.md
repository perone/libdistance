# libdistance - Library for distance calculation

This is a C++ library for distance calculation, for the moment, only
**Hamming Distance** metric is implemented.

This project also contains a command-line that exposes the distance
metrics, so you can use these metrics to calculate distance 
on the command-line shell.

# Compiling and installation

This section describes how to compile, install and render docs for the
library.

## Requirements

This library uses Google Tests for doing integation and unit testing and
it also uses some boost libraries for process manipulation and command-line
program options parsing. The libary also requires doxygen to render the
documentation and CMake as building system.

If you are using Ubuntu, you can just install the following packages below:

    * libboost-all-dev
    * libgtest-dev
    * doxygen
    * cmake

With the following command:

    sudo apt-get install libboost-all-dev libgtest-dev doxygen cmake

For some reason, Ubuntu doesn't install Google Test library files,
so you'll need to install manually unfortunatelly:

    cd /usr/src/gtest
    sudo cmake .
    sudo make
    sudo mv libg* /usr/lib/

See [this reference](http://askubuntu.com/questions/145887/why-no-library-files-installed-for-google-test)
for more information.

## Compilation

After that, create a new folder called `build` and use `cmake` to build
the library, command-line application, examples and documentation:

    mkdir build && cd build
    cmake ..
    make -j4

And that is it.

## Documentation

The documentation can be renderd using the following command:
    
    make doc

After that, the documents will be available inside the directory
called `build/doxygen/html/index.html`, just open it on your
browser.

## Command-line tool

The command-line tool is called `calc_distance`, to execute it in
order to calculate the Hamming Distance between two strings, just
use the following command:

    calc_distance --hamming -s "first string" "second string"

And then the utility will output the Hamming distance metric
between the two different strings.

## Unit Tests and Integration Tests

To execute both the Unit Tests and the Integration Tests,
you can also use `cmake`:

    make test

This command will execute all the tests.

# Suggestions for improvements and Final Remarks

There are a lot of things that can be improved, to cite:

1. Add parallelization (easily parallelization)
2. More specialization for other integral types
3. Performance benchmarks
4. More distance metrics
5. Command-line utility that accepts files
6. Command-line utility that accepts stdin/pipes
7. Test coverage analsys
8. Static / dynamic analysis (coverity, valgrind, etc)
9. Vectorization for some archs such as x86




