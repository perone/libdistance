#include <distance/distance.hpp>

namespace distance
{

int hamming_distance(int arg1, int arg2)
{
	int distance = 0;
    int val = arg1 ^ arg2;

    while (val != 0)
    {
        distance++;
        val &= val - 1;
    }
    
    return distance;
}

}