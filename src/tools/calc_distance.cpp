#include <distance/distance.hpp>

#include <boost/program_options.hpp>
#include <iostream>
#include <string>
#include <vector>

namespace dt = distance;
namespace po = boost::program_options;

int main(int argc, char *argv[])
{
    po::variables_map vm;
    std::vector<std::string> string_list;
    po::options_description desc("Allowed options");

    // Add the command-line options
    desc.add_options()
        ("help",
            "help message")
        ("hamming",
            po::value<bool>()->implicit_value(true),
            "use Hamming Distance")
        ("strings,s",
            po::value<std::vector<std::string>>()->multitoken(), 
            "input strings");

    try
    {
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);    

        // Check for sanity
        if (vm.count("help") || !vm.count("hamming") || !vm.count("strings"))
        {
            std::cout << desc << std::endl;
            return EXIT_FAILURE;
        }

        if (vm.count("strings"))
        {
            string_list = vm["strings"].as<std::vector<std::string>>();
            if (string_list.size() != 2)
                throw po::error("you must specify two strings");
        }
    }
    catch(po::error &e)
    {
        std::cout << "Error: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    // Capture the two string arguments
    std::string str1 = string_list[0];
    std::string str2 = string_list[1];

    // Different sizes, uses the minimum size
    if (str1.size() != str2.size())
    {
        std::cout << "Strings with different size, using the mininum "
                  "length for distance calculation." << std::endl
                  << std::endl;
    }

    // Calculate hamming distance
    const int min_size = std::min(str1.size(), str2.size());
    const int dvalue = dt::hamming_distance(std::begin(str1),
                                            std::begin(str2),
                                            min_size);
    std::cout << "Distance: "
              << dvalue << std::endl;
    return EXIT_SUCCESS;
}
