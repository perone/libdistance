/** @file */
#pragma once

#include <numeric>
#include <functional>

namespace distance 
{

/**
 * This is a template implementation for the Hamming Distance calculation.
 * It can be used on many different C++ types (including vectors of 
 * integral types).
 *
 * @param arg1 The first argument for calculation.
 * @param arg2 The second argument for the calculation.
 * @param size The size of the smaller argument parameter.
 * @return The Hamming distance.
 */
template<typename T>
int hamming_distance(T arg1, T arg2, int size)
{
	if (size <= 0)
		throw std::invalid_argument("invalid input size");

	auto plus_fn = std::plus<int>();
	auto inequality_fn = std::not_equal_to<int>();
    
    return std::inner_product(arg1, arg1 + size,
    	                      arg2, 0,
                              plus_fn, inequality_fn);
}


/**
 * This is a specializaiton of the hamming distance for the case
 * where the type is an integral integer type.
 *
 * @param arg1 The first integer value.
 * @param arg2 The second integer value.
 * @return The Hamming distance.
 */
int hamming_distance(int arg1, int arg2);

}